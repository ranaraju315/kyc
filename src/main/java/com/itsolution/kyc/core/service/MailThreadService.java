package com.itsolution.kyc.core.service;

import com.itsolution.kyc.constants.EmailConstant;
import com.itsolution.kyc.dto.EmailDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.io.IOException;

@Service
public class MailThreadService {

    private final Logger logger = LoggerFactory.getLogger(MailThreadService.class);

    private final MailSenderService mailSenderService;

    @Value("${bank.name}")
    private String bankName;

    @Value("${bank.website}")
    private String bankWebsite;

    public MailThreadService(@Autowired MailSenderService mailSenderService) {

        this.mailSenderService = mailSenderService;
    }

    public void sendMail(EmailDto email) {
        new Thread(() -> {
            try {
                mailSenderService.sendMailWithAttachmentBcc(email);
            } catch (Exception e) {
                logger.error("error sending email", e);
            }
        }).start();
    }

    public void sendMain(EmailConstant.Template template, EmailDto email) {
        new Thread(() -> {
            try {
                mailSenderService.send(template, email);
            } catch (Exception e) {
                logger.error("error sending email", e);
            }
        }).start();
    }

    public void testMail(EmailDto email) {

        try {
            mailSenderService.sendMailWithAttachmentBcc(email);
        } catch (MessagingException e) {
            logger.error("error sending email", e);
        } catch (IOException e) {
            logger.error("error sending email", e);
        }
    }

    public void testMail(EmailConstant.Template template, EmailDto email) {
        email.setBankName(bankName);
        email.setBankWebsite(bankWebsite);
        mailSenderService.send(template, email);


    }
}

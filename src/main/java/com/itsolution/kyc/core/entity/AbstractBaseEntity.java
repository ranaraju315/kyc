package com.itsolution.kyc.core.entity;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.AbstractPersistable;

import com.itsolution.kyc.constants.AppConstant;

/**
 * @author Sunil Babu Shrestha on 6/28/2020
 */
@Getter
@Setter
@NoArgsConstructor
public class AbstractBaseEntity<PK extends Serializable> extends AbstractPersistable<PK> {

    @CreatedDate
    @Column(name = "created_at", nullable = false, updatable = false)
    @JsonFormat(pattern = AppConstant.DATE_TIME_FORMAT)
    private LocalDate createdAt;
    @LastModifiedDate
    @Column(name = "last_modified_at", nullable = false)
    @JsonFormat(pattern = AppConstant.DATE_TIME_FORMAT)
    private LocalDate lastModifiedAt;

    @CreatedBy
    private Integer createdBy;
    @LastModifiedBy
    private Integer modifiedBy;

    @Override
    public void setId(PK id) {
        super.setId(id);
    }

}

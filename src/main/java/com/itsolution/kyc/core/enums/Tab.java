package com.itsolution.kyc.core.enums;

/**
 * @author Sunil Babu Shrestha on 7/16/2020
 */
public enum Tab {
    HOME("home"), VIEW("view");


    private final String name;

    Tab(String name) {
        this.name = name;
    }

    public static Tab getFromIndex(int index) {
        Tab[] tabs = Tab.values();
        for (int i = 0; i < tabs.length; i++) {
            if (index == i) {
                return Tab.values()[i];
            }
        }
        throw new RuntimeException("No Enum Found.");
    }

    public String getName() {
        return name;
    }
}

package com.itsolution.kyc.core.enums;

/**
 * @author Sunil Babu Shrestha on 7/6/2020
 */
public enum Status {

    UPDATE_LINK_SEND("KYC update link has Send"),
    VISITED_LINK("Customer has visited update Link"),
    UPDATED("Customer had updated KYC"),
    APPROVED("Bank Authorities approved KYC of Customer"),
    ACTIVE("Active"),
    IN_ACTIVE("InActive");

    Status(String s) {
    }
}

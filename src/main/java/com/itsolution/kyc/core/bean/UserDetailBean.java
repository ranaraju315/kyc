package com.itsolution.kyc.core.bean;


import com.itsolution.kyc.core.enums.Status;
import com.itsolution.kyc.entity.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

/**
 * @author Sunil Babu Shrestha on 4/21/2019
 */
public class UserDetailBean implements UserDetails {

    private final User user;

    public UserDetailBean(User user) {
        this.user = user;
    }


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {


        return AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_ADMIN");
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        return user.getUserName();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return user.getStatus() == Status.ACTIVE;
    }


    public User getUser() {
        return user;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof UserDetailBean) {
            return ((UserDetailBean) obj).getUser().getId().equals(user.getId());
        }
        return false;
    }


    @Override
    public int hashCode() {
        return Integer.valueOf(user.getId() + "");
    }

}
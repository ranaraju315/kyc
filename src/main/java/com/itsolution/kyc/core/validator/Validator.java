package com.itsolution.kyc.core.validator;

import org.springframework.data.util.Pair;

/**
 * @author Sunil Babu Shrestha on 7/6/2020
 */
@FunctionalInterface
public interface Validator<L, R> {

    /**
     * This pair is abstract for validation status and validation message
     *
     * @return
     */
    Pair<L, R> doValid();

    /**
     * get validation status
     *
     * @return
     */
    default L isValid() {
        Pair<L, R> pair = doValid();
        return pair.getFirst();
    }

    /**
     * get validation message
     *
     * @return
     */
    default R getValidationMsg() {
        Pair<L, R> pair = doValid();
        return pair.getSecond();
    }

}

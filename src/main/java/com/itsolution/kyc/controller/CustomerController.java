package com.itsolution.kyc.controller;

import com.itsolution.kyc.constants.AppConstant;
import com.itsolution.kyc.core.enums.Tab;
import com.itsolution.kyc.entity.Kyc;
import com.itsolution.kyc.service.KycService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Sunil Babu Shrestha on 7/1/2020
 */
@Controller
@RequestMapping(CustomerController.URI)
@RequiredArgsConstructor
public class CustomerController {

    public static final String URI = "/customer";
    private final KycService kycService;


    @GetMapping("/{nav}/main")
    public String getMainDashBoard(@PathVariable int nav, ModelMap map) {


        Tab tab = Tab.getFromIndex(nav);
        switch (tab) {
            case VIEW:
                Page<Kyc> kycs = kycService.findPaginatedPage(PageRequest.of(0, AppConstant.RECORD_PER_PAGE));
                map.put("kycs", kycs);
        }

        map.put(AppConstant.TAB, Tab.values()[nav].getName());

        return "dashboard";
    }


    @GetMapping("/kyc")
    public String getKycDetail() {

        return "kyc_customer";

    }


}

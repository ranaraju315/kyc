package com.itsolution.kyc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author Sunil Babu Shrestha on 7/16/2020
 */
@Controller
public class HomeController {

    @GetMapping("/")
    public String getMainPage() {
        return "redirect:/customer/0/main";
    }
}

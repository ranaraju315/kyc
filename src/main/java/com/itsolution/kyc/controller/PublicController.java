package com.itsolution.kyc.controller;

import com.itsolution.kyc.constants.Relationship;
import com.itsolution.kyc.dto.KycDto;
import com.itsolution.kyc.service.DistrictService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author Sunil Babu Shrestha on 7/21/2020
 */
@Controller
@RequestMapping(PublicController.URI)
@RequiredArgsConstructor
public class PublicController {
    public static final String URI = "/public/customer";
    private final DistrictService districtService;

    @GetMapping("/mykyc")
    public String getCustomerKycPage(@RequestParam(required = true) String hash, ModelMap map ) {
        KycDto kycDto = new KycDto();
        /*kycDto.setPersonalDetail(new PersonalDetailDto());*/
        map.put("relations", Relationship.values());
        map.put("districts", districtService.findAll());
        map.put("kycDto", kycDto);
        return "kyc";
    }

    @GetMapping("/search")
    public String getOnlineKycSearchPage(ModelMap map) {
        map.put("districts", districtService.findAll());
        return "online";
    }
}

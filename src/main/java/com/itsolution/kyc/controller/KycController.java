package com.itsolution.kyc.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.itsolution.kyc.constants.Relationship;
import com.itsolution.kyc.dto.KycDto;
import com.itsolution.kyc.entity.Kyc;
import com.itsolution.kyc.service.DistrictService;
import com.itsolution.kyc.service.KycService;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequiredArgsConstructor
public class KycController {

    private final DistrictService districtService;
    private final KycService kycService;

    @GetMapping("/kyc")
    public ModelAndView kyc(ModelMap map) {
        ModelAndView mv = new ModelAndView("kyc");
        KycDto kycDto = new KycDto();
        /*kycDto.setPersonalDetail(new PersonalDetailDto());*/
        mv.addObject("relations", Relationship.values());
        mv.addObject("districts", districtService.findAll());
        mv.addObject("kycDto", kycDto);
        return mv;
    }

    @RequestMapping("/map")
    public String googleMap() {
        return "google_map";
    }

    @PostMapping(value = "/kyc")
    public String kycSubmit(@ModelAttribute KycDto kycDto) {

        Kyc kyc = new Kyc();
        kyc.setAccountNumber(kycDto.getAccountNumber());
        kyc.setCardNumber(kycDto.getCardNumber());
        kyc.setCustomerName(kycDto.getPersonalDetailDto().getName());
        kyc.setCitizenshipNumber(kycDto.getPersonalDetailDto().getCitizenshipNo());
        kyc.setCitizenshipIssuingDistrict(kycDto.getPersonalDetailDto().getCitizenIssuingAuthority());
        final ObjectMapper om = new ObjectMapper();
        String jsonString = "";
        try {
            jsonString = om.writeValueAsString(kycDto);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        kyc.setData(jsonString);
        kycService.save(kyc);
        return "thankyou";
    }

}

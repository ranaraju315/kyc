package com.itsolution.kyc.dto;

import lombok.Data;

@Data
public class PoliticalInfluenceDto {
    private Boolean isInfluencePerson;
    private String influenceDescription;
}

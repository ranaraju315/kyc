package com.itsolution.kyc.dto;

import lombok.Data;

/**
 * @author Sunil Babu Shrestha on 6/28/2020
 */
@Data
public class DocumentDto {
    private String identificationDocument;
    private String addressProof;
    private String otherAddressProof;
}

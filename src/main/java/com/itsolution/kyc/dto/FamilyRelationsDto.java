package com.itsolution.kyc.dto;

import com.itsolution.kyc.entity.District;
import lombok.Data;

import com.itsolution.kyc.constants.Relationship;

/**
 * @author Sunil Babu Shrestha on 6/28/2020
 */
@Data
public class FamilyRelationsDto {
    private String fullName;
    private String contactNo;
    private String citizenshipNo;
    private District district;
    private Relationship relationship;

    public FamilyRelationsDto() {
    }

    public FamilyRelationsDto(Relationship relationship) {
        this.relationship = relationship;
    }
}

package com.itsolution.kyc.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * @author Sunil Babu Shrestha on 7/21/2020
 */
@Builder
@Data
public class EmailDto {
    private List<String> bcc;
    private String body;
    private List<String> attachment;
    private String subject;
    private String to;
    private String toName;
    private String bankName;
    private String bankWebsite;
}

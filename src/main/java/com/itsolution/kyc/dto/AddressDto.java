package com.itsolution.kyc.dto;

import com.itsolution.kyc.entity.District;
import lombok.Data;

/**
 * @author Sunil Babu Shrestha on 6/28/2020
 */
@Data
public class AddressDto {
    private String houseNo;
    private String tole;
    private String wardNo;
    private District district;

}

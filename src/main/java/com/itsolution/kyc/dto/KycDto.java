package com.itsolution.kyc.dto;

import java.util.ArrayList;
import java.util.List;

import com.itsolution.kyc.constants.Relationship;

import lombok.Data;

/**
 * @author Sunil Babu Shrestha on 6/28/2020
 */
@Data
public class KycDto {
    private String accountNumber;
    private String cardNumber;
    private DocumentDto documentDto;
    private PersonalDetailDto personalDetailDto;
    private List<FamilyRelationsDto> familyRelations;
    private List<BusinessDto> businesses;
    private AccountPurposeDto accountPurposeDto;
    private String signature;
    private String citizenshipNumber;
    private String citizenshipIssueDistrict;

}

package com.itsolution.kyc.dto;

import lombok.Data;

import java.util.List;

@Data
public class AccountPurposeDto {
    private Boolean isPoliticalInfluenced;
    private String politicalDescription;
    private String expectedMontlyTurnover;
    private String expectedMontlyTransaction;
    private String purposeOfAccount;
    private String otherAccountPurpose;
    private String sourceOfAssests;
    private String otherSourceOfAssests;
}

package com.itsolution.kyc.dto;

import lombok.Data;

/**
 * @author Sunil Babu Shrestha on 6/28/2020
 */
@Data
public class PersonalDetailDto {

    private String name;
    private String citizenshipNo;
    private String citizenIssuingAuthority;
    private String citizenIssuingDistrict;
    private String citizenIssuedDate;
    private Boolean passportObtainedFlag;
    private String passportNo;
    private String passportIssuingAuthority;
    private String passoprtIssuedDate;
    private String residenceContactNo;
    private String dob;
    private String email;
    private String mobileNo;
    private String poBoxNo;
    private String panNo;
    private AddressDto presentAddress;
    private AddressDto permanentAddress;
    private String houseOwner;
    private String permanentAddressContact;

    public PersonalDetailDto() {
    }

    public PersonalDetailDto(String name, String citizenshipNo, String citizenIssuingAuthority,
                             String citizenIssuedDate, Boolean passportObtainedFlag, String passportNo,
                             String passportIssuingAuthority, String passoprtIssuedDate, String residenceContactNo,
                             String dob, String email, String mobileNo, String poBoxNo, String panNo,
                             AddressDto presentAddress, AddressDto permanentAddress) {
        this.name = name;
        this.citizenshipNo = citizenshipNo;
        this.citizenIssuingAuthority = citizenIssuingAuthority;
        this.citizenIssuedDate = citizenIssuedDate;
        this.passportObtainedFlag = passportObtainedFlag;
        this.passportNo = passportNo;
        this.passportIssuingAuthority = passportIssuingAuthority;
        this.passoprtIssuedDate = passoprtIssuedDate;
        this.residenceContactNo = residenceContactNo;
        this.dob = dob;
        this.email = email;
        this.mobileNo = mobileNo;
        this.poBoxNo = poBoxNo;
        this.panNo = panNo;
        this.presentAddress = presentAddress;
        this.permanentAddress = permanentAddress;
    }

}

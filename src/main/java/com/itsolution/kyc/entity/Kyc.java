package com.itsolution.kyc.entity;

import com.itsolution.kyc.constants.AppConstant;
import com.itsolution.kyc.core.entity.AbstractBaseEntity;
import com.itsolution.kyc.core.enums.Status;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import java.util.List;

/**
 * @author Sunil Babu Shrestha on 7/6/2020
 */
@Entity
@Data
public class Kyc extends AbstractBaseEntity<Long> {

    private String customerName;
    private String accountNumber;
    private String cardNumber;
    private String citizenshipNumber;
    private String citizenshipIssuingDistrict;
    private String email;
    private String mobileNumber;
    private Status status;
    @Transient
    private String identifier;

    @OneToMany
    private List<Log> logs;

    @Lob
    private String data; // this will be json Data

    public String getIdentifier() {
        String encode = AppConstant.encode(String.valueOf(super.getId()));
        String uri = "/public/customer/mykyc?hash=" + encode;
        StringBuilder sb = new StringBuilder("<a href='" + uri + "'>")
                .append(uri).append("</a>");
        return sb.toString();
    }


}

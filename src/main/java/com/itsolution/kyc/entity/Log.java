package com.itsolution.kyc.entity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import lombok.Data;

import com.itsolution.kyc.core.entity.AbstractBaseEntity;

/**
 * @author Sunil Babu Shrestha on 7/6/2020
 */
@Entity
@Data
public class Log extends AbstractBaseEntity<Long> {

    @ManyToOne
    private Kyc kyc;
    private String data;

}

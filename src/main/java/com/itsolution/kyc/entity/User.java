package com.itsolution.kyc.entity;

import com.itsolution.kyc.core.entity.AbstractBaseEntity;
import com.itsolution.kyc.core.enums.Status;
import lombok.Data;

import javax.persistence.Entity;

/**
 * @author Sunil Babu Shrestha on 7/21/2020
 */
@Entity
@Data
public class User extends AbstractBaseEntity<Long> {
    private String userName;
    private String password;
    private String fullName;
    private String email;
    private Status status;

}

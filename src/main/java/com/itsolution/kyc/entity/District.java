package com.itsolution.kyc.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

/**
 * @author Sunil Babu Shrestha on 6/28/2020
 */
@Entity
@Data
public class District {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;

    public District() {
    }

    public District(String name) {
        this.name = name;
    }
}

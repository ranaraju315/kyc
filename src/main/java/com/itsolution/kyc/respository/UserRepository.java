package com.itsolution.kyc.respository;

import com.itsolution.kyc.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Sunil Babu Shrestha on 7/21/2020
 */
public interface UserRepository extends JpaRepository<User, Long> {
    User findByUserName(String userName);
}

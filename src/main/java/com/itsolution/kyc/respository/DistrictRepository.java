package com.itsolution.kyc.respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.itsolution.kyc.entity.District;
import org.springframework.stereotype.Repository;

/**
 * @author Sunil Babu Shrestha on 6/28/2020
 */
@Repository
public interface DistrictRepository extends JpaRepository<District, Long> {

}

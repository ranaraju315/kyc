package com.itsolution.kyc.respository;

import com.itsolution.kyc.entity.Kyc;
import org.springframework.data.jpa.repository.JpaRepository;

public interface KycRespository extends JpaRepository<Kyc, Long> {
}

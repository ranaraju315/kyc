package com.itsolution.kyc.constants;

import lombok.experimental.UtilityClass;

/**
 * @author Sunil Babu Shrestha on 6/28/2020
 */
@UtilityClass
public class DocConstant {

    public enum IDENTIFICATION_DOCUMENT {
        CITIZENSHIP, PASSPORT, VISA
    }

    public enum VERIFYING_DOCUMENT {
        UTILITY_BILLS, LAND_OWNERSHIP_CERTIFICATE, RENTAL_AGREEMENT, LOCAL_AUTHORITY_DOCUMENT, DRIVING_LICENCE, VOTER_ID, EMPLOYMENT_IDENTIFICATION, OTHERS
    }
}

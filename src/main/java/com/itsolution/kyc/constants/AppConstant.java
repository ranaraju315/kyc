package com.itsolution.kyc.constants;

import lombok.experimental.UtilityClass;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @author Sunil Babu Shrestha on 6/28/2020
 */
@UtilityClass
public class AppConstant {

    public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static String TAB = "tab";

    //pagination constant
    public static final int RECORD_PER_PAGE=25;

    public static String encode(String str) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.encode(str);
    }
}

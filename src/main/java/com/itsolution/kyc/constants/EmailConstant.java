package com.itsolution.kyc.constants;

import com.google.common.collect.ImmutableMap;

import java.util.Map;

/**
 * @author Sunil Babu Shrestha on 7/21/2020
 */
public final class EmailConstant {
    public static final Map<Template, String> MAIL = ImmutableMap.<Template, String>builder()
            .put(Template.KYC_UPDATE_NOTICE, "/mail/kyc-update-notice.html")
            .put(Template.KYC_APPROVED, "/mail/kyc-update-approved.html")

            .build();

    private EmailConstant() {
    }

    public enum Template {
        KYC_UPDATE_NOTICE("Notice to update KYC !!"),
        KYC_APPROVED("KYC Approved !!");

        private String subject;

        Template(String subject) {
            this.subject = subject;
        }

        public void setSubject(String subject) {
            this.subject = subject;
        }

        public String get() {
            return this.subject;
        }
    }
}

package com.itsolution.kyc.service;

import com.itsolution.kyc.entity.District;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Sunil Babu Shrestha on 6/28/2020
 */
@Service
public interface DistrictService extends BaseService<District> {
    public List<District> saveAll(List<District> districts);
}

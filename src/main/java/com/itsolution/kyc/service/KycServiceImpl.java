package com.itsolution.kyc.service;

import com.itsolution.kyc.entity.Kyc;
import com.itsolution.kyc.respository.KycRespository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Sunil Babu Shrestha on 7/8/2020
 */
@Service
@RequiredArgsConstructor
public class KycServiceImpl implements KycService {

    private final KycRespository kycRespository;

    @Override
    public List<Kyc> findAll() {
        return null;
    }

    @Override
    public Page<Kyc> findPaginatedPage(Pageable pageable) {
        return kycRespository.findAll(pageable);
    }

    @Override
    public Kyc findById(Long id) {
        return null;
    }

    @Override
    public Kyc save(Kyc kyc) {

        return kycRespository.save(kyc);
    }
}

package com.itsolution.kyc.service;

import com.itsolution.kyc.core.bean.UserDetailBean;
import com.itsolution.kyc.entity.User;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * @author Sunil Babu Shrestha on 3/8/2020
 */
@Service
@RequiredArgsConstructor
public class UserDetailServiceImpl implements UserDetailsService {


    @Autowired
    private UserService userService;
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {

        this.user = userService.findByUserName(userName);
        if (null == this.user) {
            throw new UsernameNotFoundException("User Not exists");
        }
        return new UserDetailBean(user);
    }

}
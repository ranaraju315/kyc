package com.itsolution.kyc.service;

import com.itsolution.kyc.entity.User;

/**
 * @author Sunil Babu Shrestha on 7/21/2020
 */
public interface UserService extends BaseService<User>{

    User findByUserName(String userName);
}

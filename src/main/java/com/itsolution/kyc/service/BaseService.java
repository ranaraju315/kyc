package com.itsolution.kyc.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * @author Sunil Babu Shrestha on 6/28/2020
 */
public interface BaseService<T> {

    List<T> findAll();

    Page<T> findPaginatedPage(Pageable pageable);

    T findById(Long id);

    T save(T t);

}

package com.itsolution.kyc.service;

import com.itsolution.kyc.entity.District;
import com.itsolution.kyc.respository.DistrictRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Sunil Babu Shrestha on 6/28/2020
 */
@AllArgsConstructor
@Service
public class DistrictServiceImpl implements DistrictService {

    private final DistrictRepository districtRepository;

    @Override
    public List<District> findAll() {
        return districtRepository.findAll();
    }

    @Override
    public Page<District> findPaginatedPage(Pageable pageable) {
        return districtRepository.findAll(pageable);
    }

    @Override
    public District findById(Long id) {
        return districtRepository.getOne(id);
    }

    @Override
    public District save(District district) {
        return districtRepository.save(district);
    }

    public List<District> saveAll(List<District> districts) {
        return districtRepository.saveAll(districts);
    }
}

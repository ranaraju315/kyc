package com.itsolution.kyc.service;

import com.itsolution.kyc.entity.Kyc;

/**
 * @author sushan on 7/3/20
 * @project kyc
 */
public interface KycService extends BaseService<Kyc> {

}

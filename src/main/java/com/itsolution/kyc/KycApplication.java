package com.itsolution.kyc;

import com.itsolution.kyc.core.enums.Status;
import com.itsolution.kyc.entity.District;
import com.itsolution.kyc.entity.User;
import com.itsolution.kyc.service.DistrictService;
import com.itsolution.kyc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class KycApplication {

    @Autowired
    private DistrictService districtService;
    @Autowired
    private UserService userService;

    public static void main(String[] args) {
        SpringApplication.run(KycApplication.class, args);
    }

    @PostConstruct
    public void initialize() {
        if (userService.findByUserName("admin") == null) {
            User user = new User();
            user.setFullName("The Administrator");
            user.setUserName("admin");
            user.setPassword("123456");
            user.setStatus(Status.ACTIVE);
            userService.save(user);
        }
    }

    @PostConstruct
    public void insertDistrict() {
        List<District> districtList = districtService.findAll();
        if (districtList.size() == 0) {
            List<District> districts = new ArrayList<>();
            districts.add(new District("Achham"));
            districts.add(new District("Arghakhanchi"));
            districts.add(new District("Baglung"));
            districts.add(new District("Baitadi"));
            districts.add(new District("Bajhang"));
            districts.add(new District("Bajura"));
            districts.add(new District("Banke"));
            districts.add(new District("Bara"));
            districts.add(new District("Bardiya"));
            districts.add(new District("Bhaktapur"));
            districts.add(new District("Bhojpur"));
            districts.add(new District("Chitwan"));
            districts.add(new District("Daang"));
            districts.add(new District("Dadeldhura"));
            districts.add(new District("Dailekh"));
            districts.add(new District("Darchula"));
            districts.add(new District("Dhading"));
            districts.add(new District("Dhankuta"));
            districts.add(new District("Dhanusa"));
            districts.add(new District("Dolakha"));
            districts.add(new District("Dolpa"));
            districts.add(new District("Doti"));
            districts.add(new District("Gorkha"));
            districts.add(new District("Gulmi"));
            districts.add(new District("Humla"));
            districts.add(new District("Ilam"));
            districts.add(new District("Jajarkot"));
            districts.add(new District("Jhapa"));
            districts.add(new District("Jumla"));
            districts.add(new District("Kailali"));
            districts.add(new District("Kalikot"));
            districts.add(new District("Kanchanpur"));
            districts.add(new District("Kapilbastu"));
            districts.add(new District("Kaski"));
            districts.add(new District("Kathmandu"));
            districts.add(new District("Kavrepalanchok"));
            districts.add(new District("Lalitpur"));
            districts.add(new District("Lamjung"));
            districts.add(new District("Mahottari"));
            districts.add(new District("Makawanpur"));
            districts.add(new District("Manang"));
            districts.add(new District("Morang"));
            districts.add(new District("Mugu"));
            districts.add(new District("Mustang"));
            districts.add(new District("Myagdi"));
            districts.add(new District("Nawalparasi (East of Bardaghat Susta)"));
            districts.add(new District("Nawalparasi (West of Bardaghat Susta"));
            districts.add(new District("Nuwakot"));
            districts.add(new District("Okhaldhunga"));
            districts.add(new District("Palpa"));
            districts.add(new District("Panchthar"));
            districts.add(new District("Parbat"));
            districts.add(new District("Parsa"));
            districts.add(new District("Pyuthan"));
            districts.add(new District("Ramechhap"));
            districts.add(new District("Rasuwa"));
            districts.add(new District("Rautahat"));
            districts.add(new District("Rolpa"));
            districts.add(new District("Rukum (Western Part)"));
            districts.add(new District("Rukum (Eastern Part)"));
            districts.add(new District("Rupandehi"));
            districts.add(new District("Sankhuwasabha"));
            districts.add(new District("Saptari"));
            districts.add(new District("Sarlahi"));
            districts.add(new District("Sindhuli"));
            districts.add(new District("Sindhupalchok"));
            districts.add(new District("Siraha"));
            districts.add(new District("Solukhumbu"));
            districts.add(new District("Sunsari"));
            districts.add(new District("Surkhet"));
            districts.add(new District("Syangja"));
            districts.add(new District("Tanahun"));
            districts.add(new District("Taplejung"));
            districts.add(new District("Tehrathum"));
            districts.add(new District("Udayapur"));
            districts.add(new District("Bhojpur"));
            districts.add(new District("Bhojpur"));
            districtService.saveAll(districts);
        }


    }
}
